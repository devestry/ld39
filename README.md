Steve's Lab, an adventure/survival game created for the Ludum Dare 39 Jam event.
Link: https://ldjam.com/events/ludum-dare/39/steves-lab

Created by Lazaros Katiniotis and Dimitris Masouras.